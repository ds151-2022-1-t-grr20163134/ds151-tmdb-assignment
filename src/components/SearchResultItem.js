import React from "react";
import { TouchableOpacity, Image, StyleSheet, Text, View } from "react-native";

const SearchResultItem = ({ item, smallImageURL, navigation, filters, imageURL }) => {

    const getMediaType = (item) => {
        switch (filters) {
            case 'Movies':
                return 'movie'
            case 'TV':
                return 'tv'
            case 'People':
                return 'person'
            default:
                return item.media_type
        }
    }

    return (
        <View style={styles.container}>
            <TouchableOpacity
                onPress={() => navigation.navigate("Details", {
                    id: item.id,
                    mediaType: `${getMediaType(item)}`,
                    imageURL: imageURL
                })}
            >
                <Image style={styles.imageContainer} source={`${smallImageURL}${item.profile_path || item.poster_path}`} />
                <Text style={styles.name}>{(item.original_title || item.name).length > 14 ? (item.original_title || item.name).slice(0, 14) + '...' : (item.original_title || item.name)}</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        marginBottom: 15
    },
    imageContainer: {
        height: 100,
        width: 100,
        borderRadius: 100
    },
    name: {
        textAlign: 'center'
    }
})

export default SearchResultItem