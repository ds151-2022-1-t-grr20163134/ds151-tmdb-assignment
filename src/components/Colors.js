const Colors = {
    darkBlue: '#003547',
    darkGreen: '#005E54',
    lightGreen: '#C2BB00',
    salmon: '#E1523D',
    orange: '#ED8B16'
}