import React from "react";
import { StyleSheet, Text, View } from "react-native";

const MovieDetail = ({ media }) => {
    return (
        <View style={styles.container}>
            <Text style={{marginBottom: 10}}>
                <Text style={styles.title}>{media.title}</Text><Text style={styles.small}> {(media.release_date).slice(0, 4)}</Text>
            </Text>
            <Text>
                {media.overview}
            </Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        padding: 20,
    },
    title: {
        fontSize: 32,
        fontWeight: 'bold',
    },
    small: {
        fontSize: 12
    }
})

export default MovieDetail