import React from "react";
import { Image, StyleSheet, Text, View } from "react-native";

const DetailsHeader = ({imageURL}) => {
    return (
        <View style={styles.container}>
            <Image source={imageURL} style={styles.imageContainer}/>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {

    },
    imageContainer: {
        height: 500
    }
})

export default DetailsHeader