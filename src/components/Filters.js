import React from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";

const Filter = ({ filters, setFilters }) => {

    const FilterPill = ({ filter, touchHandler }) => {
        return (
            <TouchableOpacity onPress={() => touchHandler(filter)} style={[styles.filterButton, filters.includes(filter) ? styles.filterEnable : styles.filterDisable]}>
                <Text style={[filters.includes(filter) ? styles.filterTextEnable : styles.filterTextDisable]}>{filter}</Text>
            </TouchableOpacity>
        )
    }

    const toggleFilter = (filter) => {
        // let tempFilters = [...filters]
        // const index = tempFilters.indexOf(filter)
        // if(index !== -1){
        //     tempFilters.splice(index, 1)
        // } else {
        //     tempFilters.push(filter)
        // }
        // setFilters(tempFilters)
        if(filters.includes(filter)){
            setFilters([""])     
        } else {
            setFilters(filter)     
        }
    }

    return (
        <View
            style={styles.container}>
            <FilterPill filter="Movies" touchHandler={toggleFilter}/>
            <FilterPill filter="TV" touchHandler={toggleFilter}/>
            <FilterPill filter="People" touchHandler={toggleFilter}/>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        paddingTop: 5,
        paddingRight: 0,
        paddingBottom: 5,
        paddingLeft: 0,
        marginTop: 5,
        color: 'red',
    },
    filterButton: {
        paddingTop: 3,
        paddingRight: 15,
        paddingBottom: 3,
        paddingLeft: 15,
        borderRadius: 20,
    },
    filterEnable: {
        backgroundColor: '#005E54',
        borderWidth: 2,
        borderColor: '#005E54',
    },
    filterDisable: {
        borderWidth: 2,
        borderColor: '#005E54',
    },
    filterTextEnable: {
        color: '#fff',
    },
    filterTextDisable: {
        color: '#005E54'
    }
})

export default Filter;