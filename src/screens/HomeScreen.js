import React, { useState, useEffect } from 'react';
import { View, Text, FlatList, StyleSheet, TouchableOpacity } from 'react-native';
import SearchBar from '../components/SearchBar';
import tmdb from '../api/tmdb';
import Filter from '../components/Filters';
import SearchResultItem from '../components/SearchResultItem';

const HomeScreen = ({ navigation }) => {
  const [text, setText] = useState('');
  const [results, setResults] = useState([]);
  const [filters, setFilters] = useState([""]);
  // CONFIGURATION
  const [smallImageURL, setSmallImageURL] = useState("")
  const [imageURL, setImageURL] = useState("")

  useEffect(() => {
    searchTmdb(text)
  }, [filters]);

  useEffect(() => {
    searchTmdb('fight club')
    getConfiguration()
  }, [])

  async function searchTmdb(query) {
    if (query == '' || query == null || query == undefined)
      return

    let search
    switch (filters) {
      case 'Movies':
        search = 'movie'
        break
      case 'TV':
        search = 'tv'
        break
      case 'People':
        search = 'person'
        break
      default:
        search = 'multi'
        break
    }
    try {
      const response = await tmdb.get(`/search/${search}`, {
        params: {
          query,
          include_adult: false,
        }
      })
      setResults(response.data.results);
    }
    catch (err) {
      console.log(err);
    }
  }

  const getConfiguration = async () => {
    try {
      const response = await tmdb.get(`/configuration`)
      setSmallImageURL(response.data.images.base_url + response.data.images.poster_sizes[2])
      setImageURL(response.data.images.base_url + response.data.images.poster_sizes[3])
    }
    catch (err) {
      console.log(err);
    }
  }

  return (
    <View style={styles.container}>
      <SearchBar
        onChangeText={(t) => setText(t)}
        onEndEditing={(t) => searchTmdb(t)}
        value={text}
      />
      <Filter filters={filters} setFilters={setFilters} />
      <FlatList
        style={styles.listContainer}
        data={results}
        keyExtractor={item => `${item.id}`}
        numColumns={3}
        columnWrapperStyle={{ justifyContent: 'space-between' }}
        renderItem={({ item }) =>
          <SearchResultItem item={item} smallImageURL={smallImageURL} navigation={navigation} filters={filters} imageURL={imageURL} />
        }
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    justifyContent: 'center'
  },
  listContainer: {
    display: 'flex',
    paddingTop: 15,
    paddingRight: 15,
    paddingBottom: 10,
    paddingLeft: 15,
  }
});

export default HomeScreen;
