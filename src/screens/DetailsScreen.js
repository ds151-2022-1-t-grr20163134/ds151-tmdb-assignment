import React, { useState, useEffect } from 'react';
import { Text, StyleSheet } from 'react-native';
import tmdb from '../api/tmdb';
import DetailsHeader from '../components/common/DetailsHeader';
import MovieDetail from '../components/MovieDetail';
import PeopleDetail from '../components/PeopleDetail';
import TvDetail from '../components/TvDetails';

const DetailsScreen = ({ navigation, route }) => {
  const [media, setMedia] = useState({});
  const [imageURL, setImageURL] = useState("")
  const [isLoading, setIsLoading] = useState(true)

  useEffect(() => {
    getMedia(route.params.id, route.params.mediaType);
    setImageURL(route.params.imageURL)
  }, []);
  
  async function getMedia(id, mediaType) {
    try {
      const response = await tmdb.get(`/${mediaType}/${id}`)
      await setMedia(response.data);
      setIsLoading(false)
    }
    catch (err) {
      console.log(err);
    }

  }
  const getBody = () => {
    switch (route.params.mediaType) {
      case 'movie':
        return <MovieDetail media={media} />
      case 'tv':
        return <TvDetail media={media} />
      case 'person':
        return <PeopleDetail media={media} />
      default:
        return <Text>Incorrect mediaType!</Text>
    }
  }

  if(isLoading){
    return (
      <Text>Loading</Text>
    )
  } else {
    return (
      <>
        <DetailsHeader imageURL={`${imageURL}${media.poster_path || media.profile_path}`}/>
        {getBody()}
      </>
    )
  }

}

const styles = StyleSheet.create({});

export default DetailsScreen;
