## TODO
- CSS details screen(Tv body)
- Placeholder for results with no image

## DOING (1)
- CSS details screen(People body)

## DONE
- Show poster/profile on details screen
- Custom Search(with filter)
- Fix filter (one at a time)
- CSS search results list
    - Create component
- CSS details header
- CSS details screen(Movie Body)

## FIX ME
- search only where ther is a valid query [OK]
- after change filter with empty text field, clicking on result sends to wrong result details page [2DO]
- scroll view 